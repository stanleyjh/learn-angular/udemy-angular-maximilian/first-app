import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'; // Typescript feature added

import { AppComponent } from './app.component';

// Angular Module
@NgModule({
  declarations: [AppComponent],
  // Here, we are telling Angular we want to import some features.
  imports: [BrowserModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
