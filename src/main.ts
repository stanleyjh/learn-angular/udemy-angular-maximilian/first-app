import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch((err) => console.error(err));

// expliciting the type
let name: string;

name = 'john';

console.log(name);

// impliciting the type so typescript will infer this is an integer.
let hundred: any = 100;

hundred = 'test';

console.log(hundred);
